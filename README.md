Test server Yocto layer
----

This layer is used for creating test server nodes.

Here you'll find a post with an example of how this layer can be used.

https://www.stupid-projects.com/devops-for-embedded-part-3/

## Usage
In order to use this layer you need a BSP layer. Since this layer
is made for the blog mentioned above, the usage guide is specific
for this case.

Prepare the base folder struct:
```sh
mkdit -p yocto-testserver/sources
cd yocto-testserver
```

You have two options, one it to run the `meta-test-server/scripts/setup-project.sh`
script which will prepare everythin and also build the Docker image
from the Dockerfile. The other option is to follow these manual steps:

```sh
cd sources
git clone --depth 1 https://dimtass@bitbucket.org/dimtass/meta-test-server.git
git clone --depth 1 https://dimtass@bitbucket.org/dimtass/meta-allwinner-hx.git
git clone --depth 1 -b zeus https://git.yoctoproject.org/git/poky
git clone --depth 1 -b zeus https://github.com/openembedded/meta-openembedded.git
git clone --depth 1 -b zeus https://git.yoctoproject.org/git/meta-virtualization

cd ../
cp sources/meta-allwinner-hx/scripts/setup-environment.sh .
cp sources/meta-allwinner-hx/scripts/list-machines.sh .
cp sources/meta-allwinner-hx/Docker/Dockerfile .
cp sources/meta-test-server/scripts/flash_sd.sh .
```

Then build the Docker image and run a container:
```sh
# Build docker image
docker build --build-arg userid=$(id -u) --build-arg groupid=$(id -g) -t testserver-yocto-image .
# Run docker image
docker run -it --name testserver-builder -v $(pwd):/docker -w /docker testserver-yocto-image bash
```

The above command will get in the container's bash prompt (if it doesn't you may
need to press the Enter key a couple of times). Then you need build the Yocto image
depending your BSP. In this case I'll use the Nanopi k1 Plus.

```sh
DISTRO=allwinner-distro-console MACHINE=nanopi-k1-plus source ./setup-environment.sh build
bitbake allwinner-console-image
```

## Flashing the image
After the image is built without errors then from the root folder run this command:

```sh
sudo MACHINE=nanopi-k1-plus IMAGE=console ./flash_sd.sh /dev/sdX
```

Of course, you need to change `/dev/sdX` with the proper path of your SD card.