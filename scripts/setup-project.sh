#!/bin/bash

# Check if sources/meta-test-server folder exists
if [ ! -d sources/meta-test-server ]; then
    echo "Could not found the needed sources/meta-test-server folder, exiting..."
    exit 1
fi

# Get in the sources folder
cd sources

# git clone all the needed repos
git clone --depth 1 https://dimtass@bitbucket.org/dimtass/meta-allwinner-hx.git
git clone --depth 1 -b zeus https://git.yoctoproject.org/git/poky
git clone --depth 1 -b zeus https://github.com/openembedded/meta-openembedded.git
git clone --depth 1 -b zeus https://git.yoctoproject.org/git/meta-virtualization

cd ../

# Copy the needed scripts to the root folder
cp sources/meta-allwinner-hx/scripts/setup-environment.sh .
cp sources/meta-allwinner-hx/scripts/list-machines.sh .
cp sources/meta-allwinner-hx/Docker/Dockerfile .
cp sources/meta-test-server/scripts/flash_sd.sh .

# Build the Docker image
docker build --build-arg userid=$(id -u) --build-arg groupid=$(id -g) -t testserver-yocto-image .

# Run docker image
docker run -it --name testserver-builder -v $(pwd):/docker -w /docker testserver-yocto-image bash
