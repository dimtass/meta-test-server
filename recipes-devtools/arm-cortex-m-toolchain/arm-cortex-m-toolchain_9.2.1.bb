# https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads
SUMMARY = "The official ARM toolchain for Cortex-M/R MCUs"
AUTHOR = "Dimitris Tassopoulos <dimtass@gmail.com>"

LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${WORKDIR}/${TOOLCHAIN_NAME}/share/doc/gcc-arm-none-eabi/license.txt;md5=c18349634b740b7b95f2c2159af888f5"

TOOLCHAIN_NAME = "gcc-arm-none-eabi-9-2019-q4-major"
TOOLCHAIN_ARCH_x86-64 = "x86_64"
TOOLCHAIN_ARCH_aarch64 = "aarch64"
TOOLCHAIN_HOST_OS = "linux"

SRC_URI = " \
	https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/RC2.1/gcc-arm-none-eabi-9-2019-q4-major-${TOOLCHAIN_ARCH}-linux.tar.bz2;name=toolchain \
	"
# SRC_URI[toolchain.md5sum] = "fe0029de4f4ec43cf7008944e34ff8cc" # x86-64
SRC_URI[toolchain.md5sum] = "0dfa059aae18fcf7d842e30c525076a4"

do_install() {
	install -d ${D}/opt/toolchains
	cp -r ${WORKDIR}/${TOOLCHAIN_NAME} \
			${D}/opt/toolchains/
}

FILES_${PN} += " \
	/opt/toolchains/* \
"
INSANE_SKIP_${PN} = "already-stripped arch dev-so file-rdeps staticdev"