#!/bin/bash

# This environment setup scrips is not meant to run individually
# and it should be called from another setup-environment script.
# That way this meta layer can apply its custom configurations.
#
# Author: Dimitris Tassopoulos <dimtass@gmail.com>

REL_PATH=$1

# Override bblayers
echo "Replacing bblayers.conf in ${BBPATH}/conf/bblayers.conf"
cp $REL_PATH/conf/bblayers.conf ${BBPATH}/conf/bblayers.conf

# Add params to local conf
localconf="${BBPATH}/conf/local.conf"
cat >>"${localconf}" <<EOF

## Start: added by meta-test-server/scripts/setup-environment-append.sh
DISTRO_FEATURES_append = " virtualization"
## End

EOF

#Get available images for this layer
LIST_IMAGES=$(ls $REL_PATH/recipes-core/images/*.bb | sed s/\.bb//g | sed -r 's/^.+\///' | xargs -I {} echo -e "\t"{})
cat <<EOF

These are the extra images that you can build from this BSP layer:
	${LIST_IMAGES}

EOF