SUMMARY = "Allwinner console image with I2C and SPI tools"
LICENSE = "MIT"
AUTHOR = "Dimitris Tassopoulos <dimtass@gmail.com>"

inherit allwinner-base-image

# Add our custom tools
IMAGE_INSTALL += " \
    packagegroup-test-server \
"

EXTRA_USERS_PARAMS = " \
    addgroup gpio; \
    usermod -aG gpio root; \
"