SUMMARY = "Package group for test-server. This depends on the meta-llwinner-hx"

require classes/package-groups.inc

PACKAGE_ARCH = "${MACHINE_ARCH}"
inherit packagegroup

RDEPENDS_${PN} = " \
    stlink \
    arm-cortex-m-toolchain \
    gitlab-runner \
    docker-ce \
    i2c-tools \
    ${DEV_SDK_INSTALL} \
    ${TEST_TOOLS} \
    ${PYTHON3_PKGS} \
    python3-robotframework \
    python3-robotframework-seriallibrary \
    python3-smbus \
"

RDEPENDS_${PN}_remove = " \
    ${PYTHON2_PKGS} \
"