SUMMARY = "stm32 discovery line linux programmer"
DESCRIPTION = "Recipe created by bitbake-layers"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a119cf258a06d6a2c7f4da37ca03e341"

inherit cmake

DEPENDS += "cmake-native libusb-compat"
RDEPENDS_${PN} = "libusb1"

S = "${WORKDIR}/git"

SRC_URI = " \
    git://github.com/texane/stlink.git \
"
SRCREV = "3690de9fddeed49d304a1afab6410ceb64c70e1b"

FILES_${PN} = " \
    ${libdir}/libstlink.so.1.5.1 \
    ${libdir}/libstlink.so.1 \
    ${bindir}/st-info \
    ${bindir}/st-util \
    ${bindir}/st-flash \
    ${sysconfdir}/udev \
    ${sysconfdir}/modprobe.d \
    ${sysconfdir}/udev/rules.d \
    ${sysconfdir}/udev/rules.d/49-stlinkv2-1.rules \
    ${sysconfdir}/udev/rules.d/49-stlinkv1.rules \
    ${sysconfdir}/udev/rules.d/49-stlinkv2.rules \
    ${sysconfdir}/udev/rules.d/49-stlinkv3.rules \
    ${sysconfdir}/modprobe.d/stlink_v1.conf \
"