#!/bin/bash
: ${TOKEN:=}
: ${REPO:=}
: ${CONF:=/etc/gitlab-runner/config.toml}
: ${EXEC:=/usr/bin/gitlab-runner}

set -x

function register() {
    echo "Registering a new gitlab runner for ${REPO} with token: ${TOKEN}"
    ${EXEC} register \
        --non-interactive \
        --limit 1 \
        --name ${REPO} \
        --url https://gitlab.com \
        --registration-token ${TOKEN} \
        --executor shell

    if [ $? -ne 0 ]; then
        echo "Failed to register a new runner with error: $?"
        exit 1
    fi
}

if [ -z ${TOKEN} ]; then
    echo "TOKEN is empty"
    exit 1
fi

if [ -z ${REPO} ]; then
    echo "REPO is empty"
    exit 1
fi

# Check for existing configuration
if [ ! -f ${CONF} ]; then
    echo "No configuration file found."
    register
fi

# Verify runner
${EXEC} verify --delete

# Check if the runner exists
cat ${CONF} | grep -E "name = \"${REPO}\""
if [ $? -ne 0 ]; then
    register
fi

echo "There is a valid registered runner for this repo"
${EXEC} run