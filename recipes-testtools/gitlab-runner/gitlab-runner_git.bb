# https://gitlab-runner-downloads.s3.amazonaws.com/4870-distribute-arm64-binaries/index.html
SUMMARY = "Gitlab runner client"
AUTHOR = "Dimitris Tassopoulos <dimtass@gmail.com>"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd
RDEPENDS_${PN} += "bash"

BINARY_x86-64 = "gitlab-runner-linux-amd64"
BINARY_arm = "gitlab-runner-linux-arm"
BINARY_aarch64 = "gitlab-runner-linux-arm64"

GITLAB_REPO ?= ""
GITLAB_TOKEN ?= ""

SRC_URI = " \
	https://gitlab-runner-downloads.s3.amazonaws.com/4870-distribute-arm64-binaries/binaries/${BINARY};name=binary \
	file://initialize-gitlab-runner.sh \
	file://gitlab-runner.service \
	"
SRC_URI[binary.md5sum] = "6e360dd47978866ceeea0838f9f90244"

S = "${WORKDIR}/git"

do_install () {
	install -d ${D}${systemd_system_unitdir}
	install -m 0644 ${WORKDIR}/gitlab-runner.service ${D}${systemd_system_unitdir}

	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/${BINARY} ${D}${bindir}/gitlab-runner
	install -m 0755 ${WORKDIR}/initialize-gitlab-runner.sh ${D}${bindir}/initialize-gitlab-runner.sh

	# insert token and repo in the script
	sed -i 's/REPO:=/REPO:=${GITLAB_REPO}/g' ${D}${bindir}/initialize-gitlab-runner.sh
	sed -i 's/TOKEN:=/TOKEN:=${GITLAB_TOKEN}/g' ${D}${bindir}/initialize-gitlab-runner.sh
}

FILES_${PN} += " \
	${bindir}/gitlab-runner \
	${bindir}/initialize-gitlab-runner.sh \
	${systemd_system_unitdir}/gitlab-runner.service \
"
INSANE_SKIP_${PN} = "already-stripped"

NATIVE_SYSTEMD_SUPPORT = "1"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "gitlab-runner.service"

python() {
    if not d.getVar('GITLAB_REPO'):
        bb.fatal("GITLAB_REPO is not set! Please set it in your local.conf.")

    if not d.getVar('GITLAB_TOKEN'):
        bb.fatal("GITLAB_TOKEN is not set! Please set it in your local.conf.")
}
